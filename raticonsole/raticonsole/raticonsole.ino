/*
 Console usage for Arduino projects
 Language: Wiring/Arduino

 Description

   The circuit:
 * potentiometers attached to analog inputs 0 and 1
 * pushbutton attached to digital I/O 2

 Created 24 June 2017
 by Dean Tate

 This example code is in the public domain.

 http

*/
#include "Arduino.h"
#include "fan.h"
#include "ledClass.h"

const char menuItems[] = {"LED\0Motor\0Button\0\0"};
ledClass led = ledClass();

void setup() {
  // start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Hello Raticonsole\n");
  led.init();
}

void loop() {
  fan.listAll();
  char rawArgs[20] = {'\0'};
  int r = 0;
  char waitforit = '\0';
  while(true) {
      while(!Serial.available()) { ; } 
      waitforit = Serial.read();
      if(waitforit == '\r') break;
      rawArgs[r] = waitforit;
      Serial.print(rawArgs[r]);
      ++r;
      if(r==20) r=0;
  }
  Serial.println();
  for(int i=0; i<20; ++i){
    Serial.print(rawArgs[i]);
  }
  Serial.println();

  boolean pwrstate = false;
  int ledNumber = 0;
  for(int i=0; i<20; ++i){
    if(rawArgs[i] == ' '){
      ledNumber = (int)rawArgs[i+1] - 48;
      pwrstate = (int)rawArgs[i+3] - 48;
      break;
    }
  }

  switch (1){
    case 1:
      led.pwr(ledNumber, pwrstate);
//      fan.LEDman();
      break;
    case 2:
      break;
    default:
      Serial.println("Error, bad option");
      break;
  }
}









/*
void printMenu(void){
  Serial.println();
  Serial.println(sizeof(menuItems));
  char *singleChar = menuItems;
  int i = 0;
  boolean set1 = false;
  for(int j=0; j<sizeof(menuItems); ++j){
    
    if(singleChar == '\0' && set1 == true){
      break;
    }else if(singleChar == '\0'){
      set1 = true;
    }else{
      set1 = false;
      Serial.print(i+1);
      Serial.print(". ");
      Serial.println(singleChar);
      i+=1;
    }
    singleChar += 1;
  }

}
*/
