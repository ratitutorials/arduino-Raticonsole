#ifndef ledClass_h
#define ledClass_h

#include <Arduino.h>

#define OFF HIGH
#define ON LOW


class ledClass{
  public:
    ledClass();
    void init();
    void blink(uint8_t color, uint32_t milliseconds);
    static void allOFF();  // Can be called without class object
    static void allON();
    
    // LED function patterns
    void CycleAll(uint32_t milliseconds=50);
    void Flicker(uint8_t color, uint32_t milliseconds, uint8_t repetitions);
    void pwr(int ledNumber, boolean pwrstate);
    
    // Enumeration of LED colors, allows you to call them by "led.color::red" (class.enumName::value)
    enum color{blue, green, yellow, red, white}; //EXACT order as ledClass::_pins[]
    
  private:
    static const byte _pins[];
};

//extern ledClass motor;

#endif
