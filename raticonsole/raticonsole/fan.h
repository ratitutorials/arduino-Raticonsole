
#ifndef fan_h
#define fan_h


class fclass
{
  public:
    fclass();
    void listAll();
    void LED(int ledColor, boolean pwrState); //Want to pass a pointer to a struct or another class with pre-parsed and validated options
    void LEDman(void);

};

extern fclass fan;

#endif
